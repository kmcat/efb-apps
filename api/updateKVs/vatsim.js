export default {
    async scheduled(event, env, ctx) {
      console.log(event.scheduledTime)
      ctx.waitUntil(handleSchedule(env));
    },
  }
  
  async function handleSchedule(env) {
    try{
      let headers = new Headers({
        "Accept"       : "application/json",
        "Content-Type" : "application/json",
      });
  
      async function fetchVatsimData () {
        const res = await fetch(`https://data.vatsim.net/v3/vatsim-data.json`, { method: "GET", headers })
        const json = await res.json()
        return json
      }
        
      async function fetchVatsimTransceivers () {
        const res = await fetch(`https://data.vatsim.net/v3/transceivers-data.json`, { method: "GET", headers })
        const json = await res.json()
        return json
      }
        
      const callSignToAtcType = (callsign) => {
        switch (callsign.split('_').reverse()[0]) {
          case 'CTR': return "RADAR";
          case 'DEL': return  "DELIVERY";
          case 'GND': return  "GROUND";
          case 'DEP': return "DEPARTURE";
          case 'TWR': return  "TOWER";
          case 'APP': return  "APPROACH";
          case 'ATIS': return  "ATIS";
          default: return  "UNKNOWN";
        }    
      }
  
      const callSignToType = (callsign) => {
        switch (callsign.split('_').reverse()[0]) {
          case 'CTR': return 6;
          case 'DEL': return 1;
          case 'GND': return 2;
          case 'DEP': return 4;
          case 'TWR': return 3;
          case 'APP': return 5;
          case 'ATIS': return 7;
          default: return 0;
        }
      }
        
        
      const getFrequency = (array) => {
        if (array && array.length > 0 && array[0].frequency) {
          let freqInString = array[0].frequency.toString();
          freqInString = `${freqInString.substr(0, 3)}.${freqInString.substr(3)}`;
          return parseFloat(freqInString).toFixed(3);
        }
        return null;
      }
        
      const getCenterOfCoordinates = (array)  =>{
        if (!array) return null;
        const numCoords = array.length;
        if (numCoords === 1) return [array[0].latDeg, array[0].lonDeg];
        
        let X = 0.0;
        let Y = 0.0;
        let Z = 0.0;
        
        for (let i = 0; i < numCoords; i++) {
          const lat = (array[i].latDeg * Math.PI) / 180;
          const lon = (array[i].lonDeg * Math.PI) / 180;
          const a = Math.cos(lat) * Math.cos(lon);
          const b = Math.cos(lat) * Math.sin(lon);
          const c = Math.sin(lat);
        
          X += a;
          Y += b;
          Z += c;
        }
        
        X /= numCoords;
        Y /= numCoords;
        Z /= numCoords;
        
        const lon = Math.atan2(Y, X);
        const hyp = Math.sqrt(X * X + Y * Y);
        const lat = Math.atan2(Z, hyp);
        
        const finalLat = (lat * 180) / Math.PI;
        const finalLng = (lon * 180) / Math.PI;
        
        return [finalLat, finalLng];
      }
        
        
      const _vatsimdata = await Promise.all([fetchVatsimData(), fetchVatsimTransceivers()])
      const data = _vatsimdata[0]
      const transceivers = _vatsimdata[1]
      const arr = []
      for (const c of [...data.controllers, ...data.atis]) {
        const atcType = callSignToAtcType(c.callsign);
        if (atcType !== "UNKNOWN") {
          const trans = transceivers.find((t) => t.callsign === c.callsign);
          const position = getCenterOfCoordinates(trans?.transceivers);
          const freqency = trans ? getFrequency(trans?.transceivers) : c.frequency;
            if (freqency) {
              arr.push({
                callsign: c.callsign,
                frequency: freqency,
                textAtis: c.text_atis,
                visualRange: c.visual_range,
                typeText: callSignToAtcType(c.callsign),
                type: callSignToType(c.callsign),
                latitude: position ? position[0] : null,
                longitude: position ? position[1] : null,
              });
            }
        }
  
        for (const c of arr.filter((i) => i.typeText === "ATIS" && (!i.latitude || !i.longitude))) {
          const other = arr.find((o) => o.callsign.split('_')[0] === c.callsign.split('_')[0]);
          if (other) {                
            c.latitude = other.latitude;
            c.longitude = other.longitude;
          }
        
        }   
      }
        await env.ATC_KV.put("vatsim", JSON.stringify(arr));
        return true;
    }
    catch (err) {
      console.error(err)
      return null;
    }
  
  }   