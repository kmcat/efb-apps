export default {
    async scheduled(event, env, ctx) {
      console.log(event.scheduledTime)
      ctx.waitUntil(handleSchedule(env));
    },
  }
  
  async function handleSchedule(env) {
  
    let headers = new Headers({
      "Accept"       : "application/json",
      "Content-Type" : "application/json",
    });
          
          
    async function whazzupData () {
      const res = await fetch(`https://api.ivao.aero/v2/tracker/whazzup`, { method: "GET", headers })
      const json = await res.json()
     
      return json
    }
          
    const callSignToAtcType = (callsign) => {
      switch (callsign.split('_').reverse()[0]) {
        case 'CTR': return 6;
        case 'DEL': return 1;
        case 'GND': return 2;
        case 'DEP': return 4;
        case 'TWR': return 3;
        case 'APP': return 5;
        case 'ATIS': return 7;
        default: return 0;
      }
    }
          
          
    try{
      const apiData =  await whazzupData()
      const jsonData = apiData.clients.atcs.map((atc) => ({
        callsign: atc.callsign,
        frequency: atc.atcSession.frequency.toString(),
        textAtis: atc.atis?.lines,
        latitude: atc.lastTrack.latitude,
        longitude: atc.lastTrack.longitude,
        type: callSignToAtcType(atc.callsign),
        visualRange: 100,
      }));
  
      await env.ATC_KV.put("ivao", JSON.stringify(jsonData)); 
      return true;   
    }
    catch(err) {
      console.error(err)
      return null;
      //return {  status: 503, body: "Error, with API FaaS" };
    }
          
  }