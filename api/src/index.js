import { Hono } from 'hono'
const api = new Hono()

const headers =    {
    headers: {
        'content-type': 'application/json;charset=UTF-8',
        'Cache-Control': 'max-age=300',
        'X-Robots-Tag': 'none',
        'Strict-Transport-Security': 'max-age=31536000',
        'Access-Control-Allow-Origin': '*',
    }
}

api
    .get('/atc/vatsim', async(c) => {
        let atcKv = await c.env.ATC_KV.get("vatsim");
        if (atcKv === null) return new Response("Yer... That didn't work; who knows why?", { status: 501 });
        return new Response(atcKv, headers);
    })
    .get('/atc/ivao', async(c) => {
        let atcKv = await c.env.ATC_KV.get("ivao");
        if (atcKv === null) return new Response("Yer... That didn't work; who knows why?", { status: 501 });
        return new Response(atcKv, headers);
    })

export default api



