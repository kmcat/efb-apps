import typescript from 'rollup-plugin-typescript2';
import resolve from '@rollup/plugin-node-resolve';
import css from 'rollup-plugin-import-css';
import json from "@rollup/plugin-json";

export default {
  input: './CheckList_APP/components/PMDGTablet.tsx',
  output: {
    dir: 'build/CheckList',
    format: 'es'
  },
  plugins: [css({ output: 'MyInstrument.css' }), resolve(), typescript(),  json()]
}