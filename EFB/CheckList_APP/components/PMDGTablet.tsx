/// <reference types="@microsoft/msfs-types/Pages/VCockpit/Core/VCockpit" />
/// <reference types="@microsoft/msfs-types/JS/SimVar" />
// File Not needed in final build 


import { FSComponent, EventBus  } from '@microsoft/msfs-sdk';
import { KMCat_CheckList_App, KMCEvent } from './CheckListComponent';

class PMDGTablet extends BaseInstrument {
  get templateID(): string {
    return 'PMDGTablet';
  }


  private readonly eventBus = new EventBus();
  //private readonly soundServer = new SoundServer(this.eventBus);

  public connectedCallback(): void {
    super.connectedCallback();

    FSComponent.render(<KMCat_CheckList_App bus={this.eventBus} />, document.getElementById('InstrumentContent'));
  }

  /*public onSoundEnd(soundEventId: Name_Z): void {
    super.onSoundEnd(soundEventId);
    this.soundServer.onSoundEnd(soundEventId);
  }*/


  public Update(): void {
    
    // Switch switch_01_73X
    const switch_01_73X = SimVar.GetSimVarValue('L:switch_01_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_01_73X', switch_01_73X); 

    // Switch switch_166_73X
    const switch_166_73X = SimVar.GetSimVarValue('L:switch_166_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_166_73X', switch_166_73X); 

    // Switch switch_167_73X
    const switch_167_73X = SimVar.GetSimVarValue('L:switch_167_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_167_73X', switch_167_73X); 

    // Switch switch_455_73X
    const switch_455_73X = SimVar.GetSimVarValue('L:switch_455_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_455_73X', switch_455_73X); 

    // Switch switch_255_73X
    const switch_255_73X = SimVar.GetSimVarValue('L:switch_255_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_255_73X', switch_255_73X); 

    // Switch switch_256_73X
    const switch_256_73X = SimVar.GetSimVarValue('L:switch_256_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_256_73X', switch_256_73X); 

    // Switch switch_63_73X
    const switch_63_73X = SimVar.GetSimVarValue('L:switch_63_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_63_73X', switch_63_73X); 

    // Switch switch_05_73X
    const switch_05_73X = SimVar.GetSimVarValue('L:switch_05_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_05_73X', switch_05_73X);

    // Switch switch_06_73X
    const switch_06_73X = SimVar.GetSimVarValue('L:switch_06_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_06_73X', switch_06_73X);

    // Switch switch_100_73X
    const switch_100_73X = SimVar.GetSimVarValue('L:switch_100_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_100_73X', switch_100_73X);

    // Switch switch_104_73X
    const switch_104_73X = SimVar.GetSimVarValue('L:switch_104_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_104_73X', switch_104_73X);

    // Switch switch_135_73X
    const switch_135_73X = SimVar.GetSimVarValue('L:switch_135_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_135_73X', switch_135_73X);

    // Switch switch_136_73X
    const switch_136_73X = SimVar.GetSimVarValue('L:switch_136_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_136_73X', switch_136_73X);

    // Switch switch_138_73X
    const switch_138_73X = SimVar.GetSimVarValue('L:switch_138_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_138_73X', switch_138_73X);

    // Switch switch_137_73X
    const switch_139_73X = SimVar.GetSimVarValue('L:switch_139_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_139_73X', switch_139_73X);

    // Switch switch_223_73X
    const switch_223_73X = SimVar.GetSimVarValue('L:switch_223_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_223_73X', switch_223_73X);

    // Switch switch_460_73X
    const switch_460_73X = SimVar.GetSimVarValue('L:switch_460_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_460_73X', switch_460_73X);

    // Switch switch_165_73X
    const switch_165_73X = SimVar.GetSimVarValue('L:switch_165_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_165_73X', switch_165_73X);

    // Switch switch_168_73X
    const switch_168_73X = SimVar.GetSimVarValue('L:switch_168_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_168_73X', switch_168_73X);

    // Switch switch_679_73X
    const switch_679_73X = SimVar.GetSimVarValue('L:switch_679_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_679_73X', switch_679_73X);

    // Switch switch_693_73X
    const switch_693_73X = SimVar.GetSimVarValue('L:switch_693_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_693_73X', switch_693_73X);

    // Switch switch_709_73X
    const switch_709_73X = SimVar.GetSimVarValue('L:switch_709_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_709_73X', switch_709_73X);

    // Switch switch_711_73X
    const switch_711_73X = SimVar.GetSimVarValue('L:switch_711_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_711_73X', switch_711_73X);

    // Switch switch_200_73X
    const switch_200_73X = SimVar.GetSimVarValue('L:switch_200_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_200_73X', switch_200_73X);

    // Switch switch_201_73X
    const switch_201_73X = SimVar.GetSimVarValue('L:switch_201_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_201_73X', switch_201_73X);

    // Switch switch_124_73X
    const switch_124_73X = SimVar.GetSimVarValue('L:switch_124_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_124_73X', switch_124_73X);

    // Switch switch_800_73X
    const switch_800_73X = SimVar.GetSimVarValue('L:switch_800_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_800_73X', switch_800_73X);

    // Switch switch_24_73X
    const switch_24_73X = SimVar.GetSimVarValue('L:switch_24_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_24_73X', switch_24_73X);

    // Switch switch_26_73X
    const switch_26_73X = SimVar.GetSimVarValue('L:switch_26_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_26_73X', switch_26_73X);

    // Switch switch_119_73X
    const switch_119_73X = SimVar.GetSimVarValue('L:switch_119_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_119_73X', switch_119_73X);

    // Switch switch_121_73X
    const switch_121_73X = SimVar.GetSimVarValue('L:switch_121_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_121_73X', switch_121_73X);

    // Switch switch_140_73X
    const switch_140_73X = SimVar.GetSimVarValue('L:switch_140_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_140_73X', switch_140_73X);

    // Switch switch_141_73X
    const switch_141_73X = SimVar.GetSimVarValue('L:switch_141_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_141_73X', switch_141_73X);

    // Switch switch_210_73X
    const switch_210_73X = SimVar.GetSimVarValue('L:switch_210_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_210_73X', switch_210_73X);

    // Switch switch_212_73X
    const switch_212_73X = SimVar.GetSimVarValue('L:switch_212_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_212_73X', switch_212_73X);

    // Switch switch_202_73X
    const switch_202_73X = SimVar.GetSimVarValue('L:switch_202_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_202_73X', switch_202_73X);
  
    //switch_696_73X
    const switch_696_73X= SimVar.GetSimVarValue('L:switch_696_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_696_73X', switch_696_73X);

    //switch_983_73X
    const switch_983_73X= SimVar.GetSimVarValue('L:switch_983_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_983_73X', switch_983_73X);

    //switch_9832_73X
    const switch_9832_73X = SimVar.GetSimVarValue('L:switch_9832_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_9832_73X', switch_9832_73X);

    //switch_346_73X
    const switch_346_73X = SimVar.GetSimVarValue('L:switch_346_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_346_73X', switch_346_73X);

   //switch_123_73X
   const switch_123_73X = SimVar.GetSimVarValue('L:switch_123_73X', 'number')
   this.eventBus.getPublisher<KMCEvent>().pub('switch_123_73X', switch_123_73X); 

   //switch_113_73X
   const switch_113_73X = SimVar.GetSimVarValue('L:switch_113_73X', 'number')
   this.eventBus.getPublisher<KMCEvent>().pub('switch_113_73X', switch_113_73X); 

    //switch_114_73X
    const switch_114_73X = SimVar.GetSimVarValue('L:switch_114_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_114_73X', switch_114_73X); 

    //switch_456_73X
    const switch_456_73X = SimVar.GetSimVarValue('L:switch_456_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_456_73X', switch_456_73X); 

    //switch_457_73X
    const switch_457_73X = SimVar.GetSimVarValue('L:switch_457_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_457_73X', switch_457_73X); 

    //switch_458_73X
    const switch_458_73X = SimVar.GetSimVarValue('L:switch_458_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_458_73X', switch_458_73X);

    //switch_4581_73X
    const switch_4581_73X = SimVar.GetSimVarValue('L:switch_4581_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_4581_73X', switch_4581_73X);

    //switch_37_73X
    const switch_37_73X = SimVar.GetSimVarValue('L:switch_37_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_37_73X', switch_37_73X);

    //switch_38_73X
    const switch_38_73X = SimVar.GetSimVarValue('L:switch_38_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_38_73X', switch_38_73X);

    //switch_39_73X
    const switch_39_73X = SimVar.GetSimVarValue('L:switch_39_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_39_73X', switch_39_73X);

    //switch_40_73X
    const switch_40_73X = SimVar.GetSimVarValue('L:switch_40_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_40_73X', switch_40_73X);

    //switch_156_73X
    const switch_156_73X = SimVar.GetSimVarValue('L:switch_156_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_156_73X', switch_156_73X);

    //switch_158_73X
    const switch_158_73X = SimVar.GetSimVarValue('L:switch_158_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_158_73X', switch_158_73X);

    //switch_688_73X
    const switch_688_73X = SimVar.GetSimVarValue('L:switch_688_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_688_73X', switch_688_73X);

    //switch_689_73X
    const switch_689_73X = SimVar.GetSimVarValue('L:switch_689_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_689_73X', switch_689_73X);

    //switch_16_73X
    const switch_16_73X = SimVar.GetSimVarValue('L:switch_16_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_16_73X', switch_16_73X);

    //switch_311_73X
    const switch_311_73X = SimVar.GetSimVarValue('L:switch_311_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_311_73X', switch_311_73X);
   
    const switch_2981_73X = SimVar.GetSimVarValue('L:switch_2981_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_2981_73X', switch_2981_73X);

    //switch_211_73X
    const switch_211_73X = SimVar.GetSimVarValue('L:switch_211_73X', 'number')
    this.eventBus.getPublisher<KMCEvent>().pub('switch_211_73X', switch_211_73X);


  }

}

registerInstrument('my-instrument', PMDGTablet);