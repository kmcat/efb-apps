/// <reference types="@microsoft/msfs-types/JS/SimVar" />
import { FSComponent, DisplayComponent, VNode, ComponentProps, EventBus, KeyEvents, KeyEventManager   } from '@microsoft/msfs-sdk';
import checklistJson from './checklist.json' 


interface MyComponentProps extends ComponentProps {
    bus: EventBus;
   
}

export interface AppEvnet {
  current_app: string;
}

export interface KMCEvent {
  switch_01_73X:number;
  switch_166_73X: number;
  switch_167_73X: number;
  switch_455_73X: number;
  switch_255_73X: number;
  switch_256_73X: number;
  switch_63_73X: number;
  switch_05_73X: number;
  switch_06_73X: number;
  switch_100_73X: number;
  switch_104_73X: number;
  switch_135_73X: number;
  switch_136_73X: number;
  switch_139_73X: number;
  switch_138_73X: number;
  switch_223_73X: number;
  switch_460_73X: number;
  switch_165_73X: number;
  switch_168_73X: number;
  switch_679_73X: number;
  switch_693_73X: number;
  switch_709_73X: number;
  switch_711_73X: number;
  switch_200_73X: number;
  switch_201_73X: number;
  switch_124_73X: number;
  switch_800_73X: number;
  switch_24_73X: number;
  switch_26_73X: number;
  switch_119_73X: number;
  switch_121_73X: number;
  switch_140_73X: number;
  switch_141_73X: number;
  switch_210_73X: number;
  switch_212_73X: number;
  switch_202_73X: number;
  switch_696_73X: number;
  switch_983_73X: number
  switch_9832_73X: number;
  switch_346_73X: number;
  switch_123_73X: number;
  switch_113_73X: number;
  switch_456_73X: number;
  switch_457_73X: number;
  switch_458_73X: number;
  switch_4581_73X: number;
  switch_114_73X: number;
  switch_453_73X: number;
  switch_37_73X: number;
  switch_38_73X: number;
  switch_39_73X: number;
  switch_40_73X: number;
  switch_156_73X: number;
  switch_157_73X: number;
  switch_158_73X: number;
  switch_688_73X: number;
  switch_689_73X: number;
  switch_16_73X: number;
  switch_311_73X: number;
  switch_2981_73X: number;
  switch_211_73X: number;
  
}



export class KMCat_CheckList_App extends DisplayComponent<MyComponentProps> {
  isdown: boolean;
 
  
  constructor(props: MyComponentProps) {
    super(props);
    this.isdown = false
  
  }

  hide(appName:any) {
  
    const appDiv = document.getElementById("kmc_checkList")!;   
    if (appName !== "KMC-checkList") return appDiv.style.display = "none";
        
    appDiv.style.display = "block";

    //open to next checklist
    const elm:HTMLElement = document.querySelector('.stdCheckListbtn[data-status="unchecked"]')!;
    elm.click();
  }


  async keys () {
    const manager = await KeyEventManager.getManager(this.props.bus);
    manager.interceptKey('SMOKE_OFF', true);
    manager.interceptKey('SMOKE_ON', true);
    // More keys here
  }

  //Debounce keyevents
  debounce (keyData:Object) {
    this.dothisKeyEvent(keyData)
    this.isdown = true
    setTimeout(() => { this.isdown = false }, 500) 
  }

  dothisKeyEvent (keyData:any) {
    if (this.isdown) return;
    //Need to check button and app
    const isApp:HTMLElement = document.querySelector('#kmc_checkList')!
    if(isApp.style.display !== "block") return;

    switch (keyData.key) {
      case 'SMOKE_OFF':
        //if(appName !== "checkListaAPP") return;
        this.checklistAction("checkedow", "overideItem")
        break;
      case 'SMOKE_ON' :
        this.checklistAction("checked", "checkkey");
        break;
    }
  }


  public async onAfterRender(node: VNode): Promise<any> {
    super.onAfterRender(node);
    
    //Key binds
    this.keys()
    this.props.bus.getSubscriber<KeyEvents>().on('key_intercept').handle(keyData => { this.debounce(keyData) })


    this.defineOnClicks()
    
    // @ts-ignore
    this.props.bus.getSubscriber().on('current_app')
    .whenChanged()
    .handle(this.hide);


    //Auto checklist Items
    checklistJson.forEach((elm:any) => {
      const checklistuuid = elm.checkListUUID
      elm.items.forEach((_item:any) => {
        if(!_item.auto) return;
     

        const momentSwitchItem = _item.momentSwitch || false

        if(!momentSwitchItem) {

          _item?.Lvar.forEach((autoItem:any) => {
            const checkListItem = _item.checklistItemUUID;
            this.props.bus.getSubscriber<KMCEvent>().on(autoItem.switchId).whenChanged().handle((e) => {

              const checkListItemElm = document.querySelector<HTMLElement>(`.kmcchecklistItem[data-checklistItemUUID="${checkListItem}"]`)

              const isCompleat = document.getElementById(checklistuuid)!.getAttribute('data-isCompleat')           
              if(isCompleat === "true") return;
              
              const isChecked = _item?.Lvar.every((switchDetails:any) => switchDetails.checkedState === SimVar.GetSimVarValue(`L:${switchDetails.switchId}`, switchDetails.datatype))
              
              if(isChecked) {
                checkListItemElm!.setAttribute('data-compleated', "true")
                checkListItemElm!.querySelectorAll('.chx').forEach(elm => { elm.setAttribute('data-status', "checked") })
                this.checklistAction("", "")
              }
              else {
                checkListItemElm!.setAttribute('data-compleated', "false")
                checkListItemElm!.querySelectorAll('.chx').forEach(elm => { elm.setAttribute('data-status', "unchecked") })
                this.checklistAction("", "")
              }
              
            }) 
              
          })
        }

        else {
          _item?.Lvar.forEach((autoItem:any) => {
            this.props.bus.getSubscriber<KMCEvent>().on(autoItem.switchId).whenChanged().handle((e) => {

              const checkListItem = _item.checklistItemUUID;
              const checkListItemElm = document.querySelector<HTMLElement>(`.kmcchecklistItem[data-checklistItemUUID="${checkListItem}"]`)
              const isChecked = _item?.Lvar.filter((switchDetails:any) => switchDetails.checkedState === SimVar.GetSimVarValue(`L:${switchDetails.switchId}`, switchDetails.datatype))
              if(isChecked.length === 0) return;
              
              checkListItemElm!.setAttribute('data-compleated', "true")
              checkListItemElm!.querySelectorAll('.chx').forEach(elm => { elm.setAttribute('data-status', "checked") })
              this.checklistAction("", "")
            
            })
          
          })
        }
      })
    })

  }

  defineOnClicks () {

    const help = document.querySelector('#kmchelpicon')?.addEventListener("click", function() {
      // @ts-ignore
      PMDGAlert.setAlertMessage("<h2>Key Bindings:</h2> You can set key bindings to check and overwrite items. Set the following key bindings:<br/> SMOKE_ON: Check items <br/> SMOKE_OFF: Overwrite items<hr/> <h2>Custom Checklist:</h2>For details on creating a custom checklist, visit: https://gitlab.com/kmcat/efb-apps");
      // @ts-ignore
      PMDGAlert.show();
    })
  
    const upperscope = this
    document.querySelectorAll<HTMLElement>('.stdCheckListbtn').forEach((elm) => {
      elm.addEventListener('click', function() {
        let action = elm.getAttribute('data-action')
        switch(action) {
          case 'resetchecklist': 
          upperscope.checklistAction("unchecked", "reset")
            break;
          case 'overrideall': 
          upperscope.checklistAction("checkedow", 'overideAll')
            break;
          case 'overrideItem':
            upperscope.checklistAction("checkedow", "overideItem")
            break;
          case 'loadChecklist':
            upperscope.loadChecklist(this)
            break;
          case 'resetAll':
            upperscope.resetAll()
            break;
          default: return null
        }
      })
    })

    document.querySelectorAll<HTMLElement>('.kmcchecklistItem').forEach((elm:HTMLElement) => {
      elm.addEventListener('click', function() {

        const checklistuuid = this.getAttribute('data-checklistuuid')!
        const auto = this.getAttribute('data-isauto')
        const isCompleat = document.getElementById(checklistuuid)!.getAttribute('data-isCompleat')

        //Do allow items to be unchecked in a compleated checklist
        if(isCompleat === "true" || auto === "true") return;
        let _switch  = this.getAttribute('data-compleated')
        _switch = _switch === "false" ? "true" : "false";
        this.setAttribute('data-compleated', _switch)
  
        this.querySelectorAll('.chx').forEach(elm => {
          let status = elm.getAttribute('data-status')
          status = status !== "unchecked" ? "unchecked" : "checked";
          elm.setAttribute('data-status', status)
        })
  
        upperscope.checklistAction("", "")
        
      })
    })

  }

  checklistAction (state:string, mode:string)  {
    const openCheckList = document.querySelector<HTMLElement>('.checklistshow')
    if(!openCheckList) return;
  
    const checkListUUID = openCheckList.getAttribute('data-checklistuuid')!
    let btnStatus = 'checked'
    
    if (mode === "overideAll") {
      
      btnStatus = 'overwrite'
  
      openCheckList.querySelectorAll<HTMLElement>('.kmcchecklistItem').forEach((elm:HTMLElement) => {
        elm.setAttribute('data-compleated', "true")
      })
  
      openCheckList.querySelectorAll<HTMLElement>('.chx').forEach((elm:HTMLElement) => {
        elm.setAttribute('data-status',state)
      })
  
    }
  
    if (mode === "reset") {
      
      btnStatus = 'unchecked'
  
      openCheckList.querySelectorAll<HTMLElement>('.kmcchecklistItem').forEach((elm:HTMLElement) => {
        elm.setAttribute('data-compleated',"false")
      })
  
      openCheckList.querySelectorAll<HTMLElement>('.chx').forEach((elm:HTMLElement) => {
        elm.setAttribute('data-status',state)
      })

      document.getElementById(checkListUUID)!.setAttribute('data-isCompleat', "false")
      this.checkoffAutoItems(checkListUUID)
    }
  
    //overideItem item when keybind is pressed
    if ( mode === "overideItem") {
     
      const nextitem = openCheckList.querySelector<HTMLElement>('[data-compleated="false"]')
      nextitem?.querySelectorAll('.chx').forEach(elm => {
        elm.setAttribute('data-status', state)
      })

      nextitem?.setAttribute("data-compleated", "true")
    
    }

    //Check item when keybind is pressed
    if ( mode === "checkkey") {
     
      const nextitem = openCheckList.querySelector<HTMLElement>('[data-compleated="false"][data-isauto=false]')
      nextitem?.querySelectorAll('.chx').forEach(elm => {
        elm.setAttribute('data-status', state)
      })

      nextitem?.setAttribute("data-compleated", "true")

    }
  
    //Check if list is compleat
    const remaining = openCheckList.querySelectorAll('[data-compleated="false"]').length
    if(remaining !== 0) return;
    const btnOfCompleatCheckList = document.querySelector<HTMLElement>(`.stdCheckListbtn[data-checklistuuid="${checkListUUID}"]`)
    btnOfCompleatCheckList!.setAttribute("data-status",btnStatus) 
    
    //Lock Check List
    document.getElementById(checkListUUID)!.setAttribute('data-isCompleat', "true")
    

    
  }

  loadChecklist (e:HTMLElement) {
    const uuid = e.getAttribute('data-checkListUUID')
    document.getElementById('checkListTitle')!.innerText = e.innerText
    const allCheckListBts = document.querySelectorAll('.stdCheckListbtn[data-action="loadChecklist"]')
      console.log(allCheckListBts)
      allCheckListBts.forEach(elm => {
        elm.classList.remove("kmcoutline");
        if(uuid == elm.getAttribute('data-checkListUUID')) return elm.classList.add("kmcoutline");
      })

    document?.querySelectorAll<HTMLElement>('.checlistUUID').forEach((elm:HTMLElement) => {
      if(elm.getAttribute('data-checklistUUID') === uuid) {
        elm.classList.add('checklistshow')
      } else{
        elm.classList.remove('checklistshow')
      }
    })
    this.checklistAction('','')
  }


  resetAll () {

    //Side btns back to gray
    document.querySelectorAll<HTMLElement>('.stdCheckListbtn').forEach((elm:HTMLElement) => {
      elm!.setAttribute("data-status","unchecked") 
    })
  
    //Set checklist item to uncompleated
    document.querySelectorAll<HTMLElement>('.kmcchecklistItem').forEach((elm:HTMLElement) => {
      elm.setAttribute('data-compleated',"false")
    })
    
    ///Set text, action, checkbox on item to uncompleated
    document.querySelectorAll<HTMLElement>('.chx').forEach((elm:HTMLElement) => {
      elm.setAttribute('data-status',"unchecked")
    })

    //Set the whole checklist to incompleat 
    document.querySelectorAll<HTMLElement>('.checlistUUID').forEach((checkListTop:HTMLElement) => {
      checkListTop.setAttribute('data-isCompleat', "false")
    })
    
    this.checkoffAutoItems("*")
  }

  checkoffAutoItems (_checklistUUID:string) {
    checklistJson.forEach((elm:any) => {
      if(elm.checkListUUID !== _checklistUUID) return;
      elm.items.forEach((_item:any) => {
        //Only apply to auto Items
        if(!_item.auto) return;
        const checkListItemElm = document.querySelector<HTMLElement>(`.kmcchecklistItem[data-checklistItemUUID="${_item.checklistItemUUID}"]`)
        // Check If checklist item can be checked
        const isChecked = _item?.Lvar.every((switchDetails:any) => switchDetails.checkedState === SimVar.GetSimVarValue(`L:${switchDetails.switchId}`, switchDetails.datatype))
        if(!isChecked) return;
        //Check off Items
        checkListItemElm!.setAttribute('data-compleated', "true")
        checkListItemElm!.querySelectorAll('.chx').forEach(elm => { elm.setAttribute('data-status', "checked") })
        this.checklistAction("", "")
        
      })
    })
  }


  cleanIshString(string: string)  { 

    // This remove most of the risk of DOM XSS
    // For some reason this part of the SDK
    return string.replace(/\&/g, '&amp;')
      .replace(/\</g, '&lt;')
      .replace(/\>/g, '&gt;')
      .replace(/\"/g, '&quot;')
      .replace(/\'/g, '&#x27;')
      .replace(/\//g, '&#x2F;');
  }
  public render(): VNode {
 
    return (
      <div id="kmc_checkList">
        <div class="row atc_io_panel addpadandhightCheck">
          <div class="col-sm-3 checklistside">
            <div class="row opt_input_heading">
              <div class="col-xs-12 col-md-12">
                <h1 class="kmctitleHead">Check List</h1>
              </div>
            </div>
            <div class="bottom kmchecklistside">
              
              <section class="bottomsec checklistbtns">
                <div class="row" id="checklistButtons">
                  { checklistJson.map((elm:any) => (
                    <button 
                      data-action="loadChecklist" 
                      data-checkListUUID={this.cleanIshString(elm.checkListUUID)} 
                      class="stdCheckListbtn"
                      data-status="unchecked"
                    >
                      {this.cleanIshString(elm.title)}
                    </button>
                  ))}

                </div>
              </section>
              <div class="flexvfill"></div>
              <section class="bottomsec">
                <div class="row">
                  <button data-action="resetchecklist" class="stdCheckListbtn btnaction">Rst Checklist</button>
                  <button data-action="overrideall"  class="stdCheckListbtn btnaction">Owr All </button>
                  <button data-action="overrideItem" class="stdCheckListbtn btnaction">Owr Item</button>
                  <button data-action="resetAll" class="stdCheckListbtn btnaction">Rst All </button>
                </div>
              </section>

            </div>
          </div>
          <div class="col-sm-9 opt-input-panel">
            <div class="row opt_input_heading">
              <div class="col-xs-12 col-md-12">
                <h1 class="kmctitleHead">
                  <span id="checkListTitle"></span>
                  <i id="kmchelpicon" class="fa-solid fa-info kmchelpicon"></i> 
                </h1>
              </div>
            </div>

            <div class="row cards-row-chechList">
              <section class='card-box-section checklistoverflow' id="listgohere">
                { checklistJson.map((checkList:any) => (
                  <section class="checlistUUID" 
                    data-checklistUUID={this.cleanIshString(checkList.checkListUUID)} 
                    id={this.cleanIshString(checkList.checkListUUID)}
                  >
  
                    { checkList.items.map((elm:any) => (

                      <div class="kmcchecklistItem" 
                        data-checklistItemUUID={this.cleanIshString(elm.checklistItemUUID)} 
                        data-checklistUUID={this.cleanIshString(checkList.checkListUUID)} 
                        data-compleated="false" 
                        data-isauto={this.cleanIshString(elm.auto.toString())}
                      >
                        <span class="kmcflex">
                          <svg class={elm.auto ? "autocheckedbox" : "uncheckedbox"} kmcbox chx data-status="unchecked" style="width: 60px; height: 60px;" viewBox="-35 -35 70 70">
                            <path class="_checkbox chx" data-status="unchecked" d="M -29 11 L -16 24 L 28 -28"  stroke-width="5px" fill="none" vector-effect="non-scaling-stroke"></path>
                          </svg>
                          <span class="kmcitem checklistItemText chx" data-status="unchecked">{this.cleanIshString(elm.item)}</span>
                          <span class="kmcfill checklistItemText chx"  data-status="unchecked"></span>
                          <span class="kmcaction checklistItemText chx" data-status="unchecked">{this.cleanIshString(elm.action)}</span>
                        </span>
                      </div>
                      
                    ))}
                  </section>
                ))}
              </section>
            </div>
          </div>
        </div>
      </div>
    );
  }
}