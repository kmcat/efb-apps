import typescript from 'rollup-plugin-typescript2';
import resolve from '@rollup/plugin-node-resolve';
import css from 'rollup-plugin-import-css';

export default {
  input: './ATC_App/components/PMDGTablet.tsx',
  output: {
    dir: 'build/ATC_App',
    format: 'es'
  },
  plugins: [css({ output: 'MyInstrument.css' }), resolve(), typescript()]
}