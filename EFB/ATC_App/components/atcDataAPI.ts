
export const getDistanceFromLatLonInNm = (lat1:number, lon1:number, lat2:number, lon2:number):number => {
    const deg2Rad = (deg:number) => deg * (Math.PI / 180);
    const R = 6371; // Radius of the earth in km
    const dLat = deg2Rad(lat2 - lat1); // deg2Rad below
    const dLon = deg2Rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
      + Math.cos(deg2Rad(lat1)) * Math.cos(deg2Rad(lat2))
      * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c * 0.5399568; // Distance in nm
}

export const visualRangeATIS = (type: number, visualRange:number ): number => {
    if (visualRange === 0 && type === 7) return 100;
    return visualRange
}

export const unicomTypeObject = () => {
    return { 
        callsign: 'UNICOM', 
        typeText:"UNICOM", 
        frequency: 122.800, 
        type: 8, 
        visualRange: 999999, 
        distance: 0, 
        latitude: 0, 
        longitude: 0, 
        textAtis: [],
        ATCText: {
            hastext: false, 
            textIndex: -1
        } 
    }
}   


export const createTypeObject = (arr:Array<any>) => {
    //let srtarr:any = {UNICOM:[], DELIVERY:[], GROUND:[], TOWER:[], DEPARTURE:[], APPROACH:[], RADAR:[], ATIS:[], UNKNOW:[]}
    let srtarr:Array<any> = [];
    arr.map((e:any) => {
        switch(e.type) {
            case 1:  
              srtarr.push({...e, typeText:"DELIVERY", icon:"fa-solid fa-truck-ramp-box"})
              break;
            case 2: 
              srtarr.push({...e, typeText:"GROUND", icon:"fa-solid fa-truck-plane"})
              break;
            case 3: 
              srtarr.push({...e, typeText:"TOWER", icon:"fa-solid fa-tower-observation"})
              break;
            case 4: 
              srtarr.push({...e, typeText:"DEPARTURE", icon:"fa-solid fa-plane-departure"})
              break;
            case 5: 
              srtarr.push({...e, typeText:"APPROACH", icon:"fa-solid fa-plane-arrival"})
              break;
            case 6: 
              srtarr.push({...e, typeText:"RADAR", icon:"fa-solid fa-satellite-dish"})
              break;
            case 7: 
              srtarr.push({...e, typeText:"ATIS", icon:"fa-solid fa-umbrella"})
              break;
            case 8: 
              srtarr.push({...e, typeText:"UNICOM", icon:"fa-solid fa-headset"})
              break;
            default:
              srtarr.push({...e, typeText:"UNKNOW", icon:"fa-solid fa-circle-question"})
        }
    })
    return srtarr
}


//

