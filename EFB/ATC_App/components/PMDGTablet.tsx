/// <reference types="@microsoft/msfs-types/Pages/VCockpit/Core/VCockpit" />
/// <reference types="@microsoft/msfs-types/JS/SimVar" />
// File Not needed in final build 


import { FSComponent, EventBus } from '@microsoft/msfs-sdk';
import { KMCat_ATC_App, KMCEvent } from './ATCComponent';

class PMDGTablet extends BaseInstrument {
  get templateID(): string {
    return 'PMDGTablet';
  }


  private readonly eventBus = new EventBus();

  public connectedCallback(): void {
    super.connectedCallback();

    FSComponent.render(<KMCat_ATC_App bus={this.eventBus} />, document.getElementById('InstrumentContent'));
  }

  public Update(): void {
       //FO
       const FOAct = SimVar.GetSimVarValue('COM ACTIVE FREQUENCY:2', 'MHz')
       this.eventBus.getPublisher<KMCEvent>().pub('FOAct_pub', FOAct);
       const FOStn = SimVar.GetSimVarValue('COM STANDBY FREQUENCY:2', 'MHz')
       this.eventBus.getPublisher<KMCEvent>().pub('FOStn_pub', FOStn);
       
       //CAP
       const COAct = SimVar.GetSimVarValue('COM ACTIVE FREQUENCY:1', 'MHz')
       this.eventBus.getPublisher<KMCEvent>().pub('COAct_pub', COAct);

       const COStn = SimVar.GetSimVarValue('COM STANDBY FREQUENCY:1', 'MHz')
       this.eventBus.getPublisher<KMCEvent>().pub('COStn_pub', COStn);

       const latlngalt = SimVar.GetSimVarValue('STRUCT LATLONALT', 'SIMCONNECT_DATA_LATLONALT');
       this.eventBus.getPublisher<KMCEvent>().pub('latlngalt', latlngalt);


  }

}

registerInstrument('my-instrument', PMDGTablet);