/// <reference types="@microsoft/msfs-types/JS/SimVar" />

import { FSComponent, DisplayComponent, VNode, ComponentProps, EventBus  } from '@microsoft/msfs-sdk';


interface MyComponentProps extends ComponentProps {
  
    bus: EventBus;
    cardDetials: object;
    key: string;
}

export class KMCat_ATC_Card extends DisplayComponent<any> {
  
    //Replace All Method 
    escapeRegExp(string:string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
      }
    _replaceAll(str: string, match: string, replacement: string): string{
         return str.replace(new RegExp(this.escapeRegExp(match), 'g'), ()=>replacement);
      }
    
    
  public render(): VNode {
    
    let {frequency, callsign, ATCText, icon} = this.props.cardDetials
   
    const _class =`grid-cards-inner ${this.props.key}`
    const _classIcon = `${icon} cardicon`
   
    frequency = Number(frequency).toPrecision(6)
  
    callsign = this._replaceAll( callsign, "_", " ")
    //Replace non Aa-Zz with nothing - removes possible XSS from API
    callsign = callsign.replace(/[^a-zA-Z ']/g, '');
   
    
    return (
        <div class={_class} >
            <div class="typeIconsClick">
                <i class={_classIcon} textindex={ATCText.textIndex}></i>
            </div>
            
            <div class="cardClick" frequency={frequency}>
                <h3 class="cardTitle" frequency={frequency}> {callsign}</h3>
                <small class="cardSubTitle" frequency={frequency}>{frequency}</small>
            </div>
        </div>
        
      );
    }

}

