/// <reference types="@microsoft/msfs-types/JS/SimVar" />

import { FSComponent, DisplayComponent, VNode, ComponentProps, EventBus } from '@microsoft/msfs-sdk';
import { KMCat_ATC_Card } from './KMCat_ATC_Card';

interface MyComponentProps extends ComponentProps {
    bus: EventBus;
   
}

export interface KMCEvent {
  COStn_pub: number;
  FOStn_pub: number;
  COAct_pub: number;
  FOAct_pub: number;
  latlngalt: object;
  
}


export interface AppEvnet {
  current_app: string;
}


export class KMCat_ATC_App extends DisplayComponent<MyComponentProps> {
 
  
  private readonly atcTypes: Array<string>;
  private readonly eventBus = new EventBus();
  private readonly atcText: Array<Array<string>>;
  private atcNetwotkLoop: any;
  private atcType: string;
  private atcData: Array<any>
  private returndistance: Number;


  constructor(props: MyComponentProps) {
    super(props);
    this.atcTypes = ["All","Delivery", "Ground", "Tower", "Departure", "Approach", "Radar", "ATIS"]
    this.atcText = []
    this.atcNetwotkLoop
    //Selected ATC type on top
    this.atcType = "ALL"
    this.atcData = []
    this.returndistance = 400
  }

  hide(appName:any) {
   
    const appDiv = document.getElementById("ATC");

    if (!appDiv) {
        return;
    }

    if (appName !== "atc") {
        appDiv.style.display = "none";
        return;
    }
    //Stop Getting data when closed
    
    appDiv.style.display = "block";
   
  }


  public async onAfterRender(node: VNode): Promise<any> {
    super.onAfterRender(node);

    this.props.bus.getSubscriber<KMCEvent>().on("latlngalt").onlyAfter(120000).handle(obj => {
      this.updateATCditance(obj)
    });

    this.props.bus.getSubscriber<AppEvnet>().on("current_app").whenChanged().handle(this.hide);
    //Set Radio Frequency
    //CAP_StandBy
    this.props.bus.getSubscriber<KMCEvent>().on('COStn_pub').whenChanged().handle((e:any)  => {
        const COStn = SimVar.GetSimVarValue('COM STANDBY FREQUENCY:1', 'MHz')
        document.getElementById('costb')!.innerText = COStn.toFixed(3)
    });
    //FO_StandBy
    this.props.bus.getSubscriber<KMCEvent>().on('FOStn_pub').whenChanged().handle((e:any)  => {
        const FOStn = SimVar.GetSimVarValue('COM STANDBY FREQUENCY:2', 'MHz')
        document.getElementById('fostb')!.innerText = FOStn.toFixed(3)
    });
    
    //CPT_Active
    this.props.bus.getSubscriber<KMCEvent>().on('COAct_pub').whenChanged().handle((e:any)  => {
        const COAct = SimVar.GetSimVarValue('COM ACTIVE FREQUENCY:1', 'MHz')
        document.getElementById('comain')!.innerText = COAct.toFixed(3)
    });    
    
    //FO_Active
    this.props.bus.getSubscriber<KMCEvent>().on('FOAct_pub').whenChanged().handle((e:any) => {
        const FOAct = SimVar.GetSimVarValue('COM ACTIVE FREQUENCY:2', 'MHz')
        document.getElementById('fomain')!.innerText = FOAct.toFixed(3)
    });


    this.defineOnClicks()

  }

  getDistanceFromLatLonInNm = (lat1:number, lon1:number, lat2:number, lon2:number):number => {
    const deg2Rad = (deg:number) => deg * (Math.PI / 180);
    const R = 6371; // Radius of the earth in km
    const dLat = deg2Rad(lat2 - lat1); // deg2Rad below
    const dLon = deg2Rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
      + Math.cos(deg2Rad(lat1)) * Math.cos(deg2Rad(lat2))
      * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c * 0.5399568; // Distance in nm
  }

  updateATCditance(obj:any) {
    if(this.atcData.length == 0) return;
    
    this.atcData.map((e:any) => {
      if(e.type == 8) return;
        e.distanceTo = this.getDistanceFromLatLonInNm(e.latitude, e.longitude, obj.lat, obj.long)
    })
    this.atcData.sort((a1, a2) => (a1.distanceTo > a2.distanceTo ? 1 : -1));
    this.makeCard()

  }

  makeCard() {
    let data = this.atcData
    let dis = this.returndistance || 400
    dis = dis == 2000 ? 999999 : dis
    data = data.filter((a) => a.distanceTo <= dis)
    document.getElementById('cardsinhere')!.innerHTML = ''
    data.map(e => {
      FSComponent.render(<KMCat_ATC_Card bus={this.eventBus} cardDetials={e} key={e.typeText} />, document.getElementById('cardsinhere'));
    })

    /*Object.keys(data).map((key:any) => {
      if(data[key].length === 0) return;
      const temp = data[key]
      temp.map((e:object) => {
          FSComponent.render(<ATC_Card bus={this.eventBus} cardDetials={e} key={key} />, document.getElementById('cardsinhere'));
      })
    })*/
    this.dynamicClicks()
  }

  async makeATCData(network:string): Promise<any> {

    // Add atcText to atcText array on constructor
    // send back the index of text
    const textIndex = (text: any): number => {
      if (text == null) return -1;
      this.atcText.push(text)
      return  this.atcText.length -1
    }

    const visualRangeATIS = (type: number, visualRange:number ): number => {
      if (visualRange === 0 && type === 7) return 100;
      return visualRange
    }

    const unicomTypeObject = () => {
      return { 
          callsign: 'UNICOM', 
          typeText:"UNICOM", 
          frequency: 122.800, 
          type: 8, 
          visualRange: 999999, 
          distanceTo: 0, 
          latitude: 0, 
          longitude: 0, 
          textAtis: [],
          ATCText: {
              hastext: false, 
              textIndex: -1
          } 
      }
    }
    
    const createTypeObject = (arr:Array<any>) => {
      //let srtarr:any = {UNICOM:[], DELIVERY:[], GROUND:[], TOWER:[], DEPARTURE:[], APPROACH:[], RADAR:[], ATIS:[], UNKNOW:[]}
      let srtarr:Array<any> = [];
      arr.map((e:any) => {
          switch(e.type) {
              case 1:  
                srtarr.push({...e, typeText:"DELIVERY", icon:"fa-solid fa-truck-ramp-box"})
                break;
              case 2: 
                srtarr.push({...e, typeText:"GROUND", icon:"fa-solid fa-truck-plane"})
                break;
              case 3: 
                srtarr.push({...e, typeText:"TOWER", icon:"fa-solid fa-tower-observation"})
                break;
              case 4: 
                srtarr.push({...e, typeText:"DEPARTURE", icon:"fa-solid fa-plane-departure"})
                break;
              case 5: 
                srtarr.push({...e, typeText:"APPROACH", icon:"fa-solid fa-plane-arrival"})
                break;
              case 6: 
                srtarr.push({...e, typeText:"RADAR", icon:"fa-solid fa-satellite-dish"})
                break;
              case 7: 
                srtarr.push({...e, typeText:"ATIS", icon:"fa-solid fa-umbrella"})
                break;
              case 8: 
                srtarr.push({...e, typeText:"UNICOM", icon:"fa-solid fa-headset"})
                break;
              default:
                srtarr.push({...e, typeText:"UNKNOW", icon:"fa-solid fa-circle-question"})
          }
      })
      return srtarr
    }
  

    try {

      const aircraftLat = SimVar.GetSimVarValue('PLANE LATITUDE', 'degree latitude');
      const aircraftLng = SimVar.GetSimVarValue('PLANE LONGITUDE', 'degree longitude');

      const res = await fetch(`https://efbapis.kmcat.uk/atc/${network}`);
      const json = await res.json()
      
      let arr = json.filter((a:any) => a.callsign.indexOf('_OBS') === -1 && parseFloat(a.frequency) <= 136.975);
      arr =  arr.map((e:any) => (
       
        {
          ...e, 
          distanceTo: Math.round(this.getDistanceFromLatLonInNm(e.latitude, e.longitude, aircraftLat, aircraftLng)),
          visualRange: visualRangeATIS(e.type, e.visualRange),
          area: e.callsign.split('_')[0],
          ATCText: {
            hastext: e.textAtis != null ? true : false, 
            textIndex: textIndex(e.textAtis)
          },
        }
      ))

      arr.sort((a1:any, a2:any) => (a1.distanceTo > a2.distanceTo ? 1 : -1));
      //arr = arr.slice(0, 50)
      //const nuItems = arr.length % 3
      //arr = arr.slice(0, (arr.length - nuItems) -1);
      arr.unshift(unicomTypeObject())
     
      return createTypeObject(arr)
    }

    catch (err) { 

      document.getElementById('atcTextHolder')!.innerText = "Opps... Issue getting ATC data";
      console.error(err) 
    } 
  }


  //Update Loop
  atcloop(type:string) {
    console.log(type)
    if (!this.atcNetwotkLoop) {
        this.atcNetwotkLoop = setInterval( () => this.getandmake(type), 600000 );
    }
  }

  //Update cards
  async getandmake(type:string) {
    const ATCdata = await this.makeATCData(type)
    this.atcData = ATCdata
    this.makeCard()
    
  }

  dynamicClicks() {

    //One day...
    // Volume slider for Vpilot or IVAO
    //SimVar.SetSimVarValue("K:COM1_VOLUME_SET", "percent", 30)
    //const a = SimVar.GetSimVarValue('COM VOLUME:1', 'percent')

    
    //ATC Text
    //ATC text is stored in "atcText" array
    //Stations with no ATC text have a index value of -1
    //When get ATC is clicked
    //Lookup index in "atcText" array

    document.querySelectorAll('.typeIconsClick').forEach(el => el.addEventListener('click', (event:any) => {
      let textIndex = event.target.getAttribute("textindex")
      if(textIndex == -1) return document.getElementById('atcTextHolder')!.innerText = "No text for this station";
      const renderText = this.atcText[textIndex].join('\n')
      //Using innerText to remove possible XSS sink from API 
      document.getElementById('atcTextHolder')!.innerText = renderText
    }))

    document.querySelectorAll('.cardClick').forEach(el => el.addEventListener('click', (event:any) => {

      //Make Radio frequency in to KHz
      let frq = event.target.getAttribute("frequency")
      frq = frq.replace('.','')
      frq = Number(frq * 1000)
        
      //Set radion on correct side
      //getTabletSide is called from PDMG EFB is not defined in this code
      //@ts-ignore
      getTabletSide() == 'FO' 
        ? SimVar.SetSimVarValue("K:COM2_STBY_RADIO_SET_HZ", "Hz" , frq) 
        : SimVar.SetSimVarValue("K:COM_STBY_RADIO_SET_HZ", "Hz" , frq);
      
    }))

    document.querySelectorAll('.cardClick').forEach(el => el.addEventListener('contextmenu', (event:any) => {
      
      event.preventDefault();
      //Make Radio frequency in to KHz
      let frq = event.target.getAttribute("frequency")
      frq = frq.replace('.','')
      frq = Number(frq * 1000)
        
      //Set radion on other side when right clicked
      //getTabletSide is called from PDMG EFB is not defined in this code
      //@ts-ignore
      getTabletSide() != 'FO' 
        ? SimVar.SetSimVarValue("K:COM2_STBY_RADIO_SET_HZ", "Hz" , frq) 
        : SimVar.SetSimVarValue("K:COM_STBY_RADIO_SET_HZ", "Hz" , frq);
      
    }))

    this.atctypefilter(this.atcType)

  }

  atctypefilter(type:string) {

    const _atcTypes = ["ATIS", "DELIVERY", "GROUND", "TOWER", "APPROACH", "DEPARTURE", "RADAR", "UNICOM", "UNKNOW"]

    if(type === "ALL") {  
      _atcTypes.map(type => { document.querySelectorAll<HTMLElement>(`.${type}`).forEach((e:HTMLElement) => e.style.display = '' )})
    }
    else {
      document.querySelectorAll<HTMLElement>(`.${type}`).forEach((e:HTMLElement) => e.style.display = '' )
      // TODO make this loopy
      let temp = _atcTypes.filter(item => item !== type)
      temp.map(type => {
        document.querySelectorAll<HTMLElement>(`.${type}`).forEach((e:HTMLElement) => e.style.display = 'none' )
      })
    }
  }


  defineOnClicks() {
    const displayvalue = (value:number) => {
      if (value >= 2000) return `Inf`;
      //@ts-ignore
      if(Settings.getUnitFromType('distance') == 'nm') {
       const nm  = value * 0.8689762
       return `${(Math.round(nm/100)*100).toLocaleString()} NM`;
      }
      else{
       const km = value * 1.609344
       return `${(Math.round(km/100)*100).toLocaleString()} km`;
      }
      
     }
   //SWAP Radios
   //CAP
    document.getElementById('CORadioTrans')!.addEventListener('click', () => 
      SimVar.SetSimVarValue("K:COM_STBY_RADIO_SWAP", "bool", true));
    //FO
    document.getElementById('FORadioTrans')!.addEventListener('click', () => 
      SimVar.SetSimVarValue("K:COM2_RADIO_SWAP", "bool", true));

    //Distnace Slider
    document.querySelector<HTMLInputElement>('#disSlider')!.addEventListener("change", (event: any) => {
      this.returndistance = event.target.valueAsNumber
  
      
      document.getElementById('disval')!.innerText = displayvalue(event.target.valueAsNumber)
      this.makeCard()
    })

    document.getElementById('disval')!.innerText = displayvalue(400)
    
    document.querySelectorAll<HTMLElement>('.remove').forEach(el => el.addEventListener('click', (event:any) => {

      const type = event.target.getAttribute("atc_type").toUpperCase();
      this.atcType = type
      const indexClicked = this.atcTypes.indexOf(event.target.getAttribute("atc_type"));
      
      let notselectedbtns = [...this.atcTypes];
      notselectedbtns.splice(indexClicked, 1)
     
      notselectedbtns.map(e => document.getElementById(`btnID_${e.toUpperCase()}`)!.style.backgroundColor = 'rgb(16, 84, 125)' );

      document.getElementById(`btnID_${this.atcTypes[indexClicked].toUpperCase()}`)!.style.backgroundColor = 'rgb(29, 160, 239)';


      this.atctypefilter(type)
  
    }));



    document.querySelectorAll('.atcbtns').forEach(el => el.addEventListener('click', (event:any) => {

      const showvol = () => document.querySelectorAll<HTMLElement>('.slidecontainer').forEach((e:HTMLElement) => e.style.display = '' );
      //const displaynone = () => document.querySelectorAll<HTMLElement>('.slidecontainer').forEach((e:HTMLElement) => e.style.display = 'none' );
      
      //Could be done better; doesn't save anything
      const btnItem = event.target.getAttribute('atc_type');
      this.atcData = []
      if (btnItem !== 'OFF') {
        //VATSIM
        if (btnItem == 'VATSIM') {
          clearInterval(this.atcNetwotkLoop);
          this.atcNetwotkLoop = null;
          this.getandmake('vatsim')
          this.atcloop('vatsim');
          document.getElementById('atcTextHolder')!.innerText = "...";
          // Network BTNS
          document.getElementById('btnID_off')!.style.backgroundColor = '#10547d'
          document.getElementById('btnID_VATSIM')!.style.backgroundColor = '#1DA0EF'
          document.getElementById('btnID_IVAO')!.style.backgroundColor = '#10547d'
          //displaynone()
        }
        //IVAO Button
        if (btnItem == 'IVAO') {
          clearInterval(this.atcNetwotkLoop);
          this.atcNetwotkLoop = null;
          this.getandmake('ivao')
          this.atcloop('ivao');
          document.getElementById('atcTextHolder')!.innerText = "...";
          // Network BTNS
          document.getElementById('btnID_off')!.style.backgroundColor = '#10547d'
          document.getElementById('btnID_VATSIM')!.style.backgroundColor = '#10547d'
          document.getElementById('btnID_IVAO')!.style.backgroundColor = '#1DA0EF'
          showvol()
        }
      }
      //Off Button
      else {
        clearInterval(this.atcNetwotkLoop);
        this.atcNetwotkLoop = null;
        document.getElementById('cardsinhere')!.innerHTML = '';
        document.getElementById('atcTextHolder')!.innerText = "...";
        // Network BTNS
        document.getElementById('btnID_off')!.style.backgroundColor = '#1DA0EF'
        document.getElementById('btnID_VATSIM')!.style.backgroundColor = '#10547d'
        document.getElementById('btnID_IVAO')!.style.backgroundColor = '#10547d'
        //displaynone()
      }
    }));
  }
  

  public render(): VNode {
 
    return (
       
      <div id="ATC">
        <div class="row">
          <div class='btn-group' role='group'>
           
            { this.atcTypes.map((type:string, index: number) => (
              <button 
                type="button"
                class="remove btn btn-primary efb_general_button opt_page_button" 
                id={`btnID_${type.toUpperCase()}`}
                atc_type={type} 
              >
                {type}
              </button>
            ))}

          </div>
        </div>

        <div class="row">
          <div class="row atc_io_panel addpaddingtoatc">
            <div class="col-sm-9 opt-input-panel">
              <div class="row opt_input_heading">
                <div class="col-xs-12 col-md-12">
                  <h1 class="titleHead">Air Traffic Control</h1>
                </div>
              </div>

              <div class="row cards-row">
                <section class='card-box-section'>
                  <div id="cardsinhere" class="grid-cards"></div>
                </section>
              </div>
              <div class="row opt_input_heading atctexttitle">
                <div class="col-xs-12 col-md-12">
                  <h1 class="titleHead">ATC Text</h1>
                </div>
              </div>
              <div class="row">
                <div class="bottom mainATCgridcards">
                  <section class="atcTextHolder">
                    <div class="atcTextoverFlow">
                      <span id="atcTextHolder" class="atcText">...</span>
                    </div>
                  </section>
                </div>
              </div>
            </div>


            <div class="col-sm-3 frqcards">
              <div class="row opt_input_heading">
                <div class="col-xs-12 col-md-12">
                  <h1 class="titleHead">Radios</h1>
                </div>
              </div>
              <div class="bottom mainATCgridcards">
                
                <section class="bottomsec">
                  <div class="row">
                    <div class='col-xs-5 col-md-5'>
                      <label class="radioType">Primary</label>
                      <span class="frequency" id="comain">121.000</span>
                    </div>
                    <div class='col-xs-1 col-md-1'></div>
                    <div class='col-xs-5 col-md-5'>
                      <label class="radioType">Standby</label>
                      <span class="frequency sdy" id="costb">118.115</span>
                    </div>
                    <button id="CORadioTrans" class="frqSwitch">TRANSFER</button>
                    <div class="ATCslidecontainer">
                      <input type="range" id="com1vol" class="efb_preferences_input slider_input" min="8" max="100" value="100" />
                    </div>
                  </div>
      
                </section>

                <section class="bottomsec">
                  <div class="row">
                    <div class='col-xs-5 col-md-5'>
                      <label class="radioType">Primary</label>
                      <span class="frequency" id="fomain">121.500</span>
                    </div>
                    <div class='col-xs-1 col-md-1'></div>
                    <div class='col-xs-5 col-md-5'>
                      <label class="radioType">Standby</label>
                      <span class="frequency sdy" id="fostb">118.115</span>
                    </div>
                      <button id="FORadioTrans" class="frqSwitch">TRANSFER</button>
                      <div class="ATCslidecontainer">
                        <input type="range" id="com2vol" class="efb_preferences_input slider_input" min="8" max="100" value="100" />
                      </div>
                  </div>
                </section>

                <section class="bottomsec">
                  <div class="row">
                    <div class='btn-group' role='group'>
                      <button 
                        type="button"
                        class="btn btn-primary efb_general_button opt_page_button atcbtns" 
                        id='btnID_off'
                        atc_type='OFF'
                        style='background-Color: rgb(16, 84, 125);'
                      >
                      OFF
                      </button>
                      <button 
                        type="button"
                        class="btn btn-primary efb_general_button opt_page_button atcbtns" 
                        id='btnID_VATSIM'
                        atc_type='VATSIM'
                      >
                       VATSIM 
                      </button>
                      <button 
                        type="button"
                        class="btn btn-primary efb_general_button opt_page_button atcbtns" 
                        id='btnID_IVAO'
                        atc_type='IVAO'
                      >
                       IVAO 
                      </button>
                    </div>
                    <div class="">
                    <label class="radioType">Distance Limit: <span id="disval"></span></label>
                      <input type="range" id="disSlider" class="efb_preferences_input slider_input" step="100" min="100" max="2000" value="400" />
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>         
    );
  }
}