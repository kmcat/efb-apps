
# ATC App for PMDG EFB

ATC App for PMDG EFB, view online VATSim and IVAO controlers. Swap your radio frequency with ease. 


<a href="https://www.buymeacoffee.com/kmcat" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

<img src="https://cdn.kmcat.uk/public/atcapp.webp" height="500px">




## Installation Easy Method 

To install my ATC App the easy way follow the link to the auto installer: [efbapps.kmcat.uk](https://efbapps.kmcat.uk).

A nice install guide and video review can be found on [YouTube by IslandSimPilot](https://youtu.be/VghXUqc6OmM?si=b593WdSfjuX7R2d9&t).

<img src="https://cdn.kmcat.uk/public/steps.gif" height="500px">



## Installation Manually 

### Step 1.
  Locate your MSFS community folder. This may be different depending on your install of MSFS, but here are some common locations of the community folder:
  
  - Steam: `%appdata%\Microsoft Flight Simulator\Packages\Community\`

  - MS-Store: `c:\users\%username%\AppData\Local\Packages\MicrosoftFlightSimulator_8wekyb3d8bbwe\LocalCache\Packages\Community`


### Step 2.
  After locating the community folder, follow the folder path below to the *pmdg-737-800* folder.
  
   `pmdg-aircraft-738\html_ui\Pages\VCockpit\Instruments\PMDGTablet\pmdg-737-800`

You should be in a folder that contains the files  `PMDGTablet.css & PMDGTablet.js` **make a copy of these files**.

### Step 3.
 Open both `PMDGTablet.css & PMDGTablet.js` in your code editor.

### Step 3a - PMDGTablet.js edits

Make the following changes / edits to the `PMDGTablet.js` file:

#### Edit 1
  On line 21403, at the end of the constructor on the HomeScreen Class

  Add a new line and add the following code:

```javascript
//KMC ATC APP
this.atcIcon = FSComponent.createRef();
this.atc = (FSComponent.buildComponent("div", { 
  id: "atc-icon", 
  class: "icon", 
  ref: this.atcIcon 
}, 
FSComponent.buildComponent("span", null, "ATC Network")));
this.atcIcon.instance.onclick = (e) => { publisher.pub('current_app', 'atc'); };
```
---
#### Edit 2

Just below where you made your last edit.

On line 21422, within the render Method on the HomeScreen Class,

Remove the code in line 21422:
```javascript
--  FSComponent.buildComponent("div", { class: "col-md-4", id: "debug-cell" })),
```
and replace it with: 
```javascript
  FSComponent.buildComponent("div", { class: "col-md-4", id: "debug-cell" }, the.atc)),
```
---
#### Edit 3
Now on line 48617, within the connectedCallback Method on the PMDGTablet Class

Add a new line and add the following code:
```javascript
//KMC ATC APP
FSComponent.render(FSComponent.buildComponent(KMCat_ATC_App, { bus: this.eventBus }), document.getElementById('AppContent'));
```
---
#### Edit 4

Just below where you made your last edit.

On line 48722, below the enableDebugMode Method on the PMDGTablet Class

Add a new line and add the following code:
```javascript
  //KMC ATC-APP
  Update() {
    const FOAct = SimVar.GetSimVarValue('COM ACTIVE FREQUENCY:2', 'MHz');
      this.eventBus.getPublisher().pub('FOAct_pub', FOAct);
    const FOStn = SimVar.GetSimVarValue('COM STANDBY FREQUENCY:2', 'MHz')
      this.eventBus.getPublisher().pub('FOStn_pub', FOStn)
    const COAct = SimVar.GetSimVarValue('COM ACTIVE FREQUENCY:1', 'MHz');
      this.eventBus.getPublisher().pub('COAct_pub', COAct);
    const COStn = SimVar.GetSimVarValue('COM STANDBY FREQUENCY:1', 'MHz');
      this.eventBus.getPublisher().pub('COStn_pub', COStn);
    const latlngalt = SimVar.GetSimVarValue('STRUCT LATLONALT', 'SIMCONNECT_DATA_LATLONALT');
      this.eventBus.getPublisher().pub('latlngalt', latlngalt);
  }

```
---
#### Edit 5

Now open the [link](https://cdn.kmcat.uk/efb-data/ATC_Networks/KMC_ATCApp2.txt) and copy all the text content.
Paste the text you've just copied at the bottom of the file, **Below** `registerInstrument('pmdg-tablet', PMDGTablet);` on line 48724.

**Save the changes** and close the `PMDGTablet.js` file.

### Step 3b - PMDGTablet.css edits
Now open the `PMDGTablet.css` file.

Open the [link](https://cdn.kmcat.uk/efb-data/ATC_Networks/atccss.txt) and copy all the text content.


Go to the bottom of the `PMDGTablet.css` file and add a new line. Paste the text you've just copied to the new line.

**Save the changes** and close the `PMDGTablet.css` file.

You are Done!!!