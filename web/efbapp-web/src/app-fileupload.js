import { LitElement, css, html } from 'lit'

import foldericon from './folder.svg'

export class APP_FileUpload extends LitElement {

    constructor() {
        super()
        this.neededFiles = ["PMDGTablet.js", "PMDGTablet.css"]
        this.neededFilesText = ["PMDGTablet.js", "PMDGTablet.css"]
        this.file = []
        this.appsToinstallStr = "[]"
        this.appsToinstall
        this.selectedText = ""
        this.shortname = ""
    }

    static properties = {
        neededFilesText: {type: Array},
        appsToinstallStr: {type: String},
        selectedText:  {type: String},
        shortname:  {type: String},
    }

    updated(changedProperties) {
        if(changedProperties.has('appsToinstallStr')) {
            console.log(this.appsToinstallStr)
            this.appsToinstall = JSON.parse(this.appsToinstallStr)
        }
    }

    addCodetoFiles(file) {
        return new Promise(async (resolve, reject) => {
          if(file?.name == 'PMDGTablet.js') {
            const {ast} = await import('./ast')
            const reader = new FileReader();
            reader.addEventListener('load', async (event) => {
              try{
          
                resolve({filename: "PMDGTablet.js", filedata: await ast(event.target.result, this.appsToinstall), originalFile:event.target.result})
              }
              catch(err) {
                console.error(err)
                reject("Unable to Connect to backend")
              }
            })
            reader.readAsText(file);
          }
          else if (file?.name == 'PMDGTablet.css') {
            const reader = new FileReader();
            reader.addEventListener('load', async (event) => {
              try{
    
                const fetchCss = async (uri) => {
                  const response = await fetch(uri)
                  if (!response.ok) throw new Error("Connection to End Failed");
                  const cssText = await response.text()
                  return cssText
                }
    
                const urls = []
                console.log(this.appsToinstall)
                this.appsToinstall.forEach(app => {
                  switch (app) {
                    case 'ATC Network':
                      urls.push('https://cdn.kmcat.uk/efb-data/ATC_Networks/atccss.txt')
                      break;
                    case 'CheckList':
                      urls.push('https://cdn.kmcat.uk//efb-data/CheckList/checklistcss.txt')
                      break;
                  }
                });
    
    
                const converedCssFiles = await Promise.allSettled(urls.map(_url => fetchCss(_url)))
                console.log(converedCssFiles)
    
                const resolved = converedCssFiles.filter(e => e.status == "fulfilled")
                const rejected = converedCssFiles.filter(e => e.status == "rejected")
      
                if(rejected.length > 0) { 
                  rejected.forEach((e) => {
                    console.warn(e.reason)
                    if(e.reason === "Unable to connect to backend") {
                      throw e.reason
                    }
                  })
                }
    
                if(resolved.length <= 0)  throw "No CSS Files from backEnd";
    
                let text = event.target.result
                resolved.forEach(cssData => {
                  text += cssData.value
                })
                
                resolve({filename:"PMDGTablet.css", filedata:text, originalFile:event.target.result})
              }
              catch(err) {
                console.log(err)
                reject("Unable to connect to backend")
              }
            })
            reader.readAsText(file);
          }
          else {
            reject(`File: ${file?.name || null} was not expected,`)
          }
        })
      }
    
      async makeZipFolder(files) {
        const {zipSync, strToU8} = await import('fflate');
    
        let atcAppFiles = {}
        files.map(e => {
          const {filename, filedata} = e.value
          atcAppFiles = { 
            ...atcAppFiles,
            [filename]: strToU8(filedata)
          }
        })
    
        let _originalFiles = {}
        files.map(e => {
          const {filename, originalFile} = e.value
          _originalFiles = { 
            ..._originalFiles,
            [filename]: strToU8(originalFile)
          }
        })
    
        const date = new Date()
        const str = `${date.getFullYear()}${date.getMonth() +1}${date.getDate()}${date.getHours()}${date.getMinutes()}`
    
        const zipped = zipSync({
            [`pmdg-aircraft-${this.shortname}`]: {
              'html_ui': {
                'Pages':{
                  'VCockpit':{
                    'Instruments':{
                      'PMDGTablet':{
                        [`pmdg-${this.selectedText}`]:{
                          ...atcAppFiles,
                          'originalFiles': {
                              [str]: {
                              ..._originalFiles
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          
        })
    
        this.fileReady(zipped)
        
      }
    
    
      fileReady(zippedFile) {
        this.file = []
        this.neededFilesText = ["PMDGTablet.js", "PMDGTablet.css"]
        this._dispatchEvent(zippedFile)
      }

      _dispatchEvent (zippedFile) {
        
        const options = {
            detail: {msg:"Read to download", blob: zippedFile},
            bubbles: true,
            composed: true,
        };
       
        this.dispatchEvent(new CustomEvent('downloadReady', options));
    }


      async ConvertFiles() {
    
        try {
          console.log("Apps to install: ", this.appsToinstall)
          if(this.appsToinstall.length === 0)  throw "No Apps selected";
      
          const btn = this.shadowRoot.querySelector('#file-uploader-button')
          btn.innerText = "Converting..."
          btn.setAttribute('disabled', 'disabled')
    
         
          //const fileuploaded2 = this.shadowRoot.querySelector("#file-uploader")
          //const files = this.file///await fileuploaded2.getFiles()
          const filearray = Array.from(this.file)
          let fileNames = []
          filearray.map(file => fileNames.push(file?.name || null))
        
          if(!fileNames.includes('PMDGTablet.js') || !fileNames.includes('PMDGTablet.css')) {
            if(!fileNames.includes('PMDGTablet.js') && !fileNames.includes('PMDGTablet.css')) throw `PMDGTablet.js & PMDGTablet.css are not uploaded`; 
            if(!fileNames.includes('PMDGTablet.js')) throw `PMDGTablet.js file is not uploaded`;
            if(!fileNames.includes('PMDGTablet.css'))  throw `PMDGTablet.css file not uploaded`;
            
            throw null;
          }
          
          //Using allSettled, just incase wrong file is uploaded
          //So the correct files will be converted - wrong one will dismissed
          const converedFiles = await Promise.allSettled(filearray.map(_file => this.addCodetoFiles(_file)))
         
          const resolved = converedFiles.filter(e => e.status == "fulfilled")
          const rejected = converedFiles.filter(e => e.status == "rejected")
        
          if(rejected.length > 0) { 
            rejected.map((e) => {
              console.warn(e.reason)
              if(e.reason === "Unable to connect to backend") {
                throw e.reason
              }
            })
          }
    
          if(resolved.length !== 0) return this.makeZipFolder(resolved, filearray);
    
          throw "No Valid Files Found"
        }
    
        catch(error) {
    
          const btn = this.shadowRoot.querySelector('#file-uploader-button')
          btn.innerText = "Convert files"
          btn.removeAttribute("disabled");
          console.error(error)
    
          const msg = this.shadowRoot.querySelector('#errormsg')
          msg.innerHTML = error
          msg.className = "show"
          setTimeout(function(){ msg.className = msg.className.replace("show", ""); }, 3000);
        }
    
        finally {
    
          const btn = this.shadowRoot.querySelector('#file-uploader-button')
          btn.innerText = "Convert files"
          btn.removeAttribute("disabled");
    
        }
        
      }
    
    
      filesInput(e) {
        const fileList = e.target?.files
        const textFiles  = new Array(...this.neededFilesText)
     
         for (const file of fileList) {
           if(this.neededFiles.includes(file?.name)){
             const index = textFiles.indexOf(file.name)
             console.log(index)
             textFiles.splice(index, 1);
             this.file.push(file)
           }
         }
         this.neededFilesText = textFiles
       }
    
    
      drop(event){
        console.log(event)
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy'; 
        const fileList = event.dataTransfer.files;
     
       
        const textFiles  = new Array(...this.neededFilesText)
       
        for (const file of fileList) {
          if(this.neededFiles.includes(file?.name)){
            const index = textFiles.indexOf(file.name)
            textFiles.splice(index, 1);
            this.file.push(file)
          }
        }
    
        this.neededFilesText = textFiles
      }
    
      dragover(event) {
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
      }
    
      dragenter(e) {
        e.preventDefault()
      }
    
      async viewfiles() {
       
        const files = this.shadowRoot.querySelector('#fileid')
        files.click()
      }


    render() { 
        
        return html`
            <div class="fw-flex fw-flex-column fw-justify-center">
                <div class="fw-flex fw-flex-column">
                    <div 
                        class="box" 
                        @dragdrop="${e => this.drop(e)}" 
                        @drop="${(e) => this.drop(e)}"  
                        @dragover=${(e) => this.dragover(e)} 
                        @dragenter=${(e) => this.dragenter(e)}
                    >
                        <div class="container">
                            <div class="uploadText">
                                <img src="${foldericon}" alt="folder Icon, for drag a drop box"><br/>
                                Drop your PMDGTablet.js & PMDGTablet.css<br/>
                                or <span @click=${(this.viewfiles)}><b><a href='#'>Select Files</a></b></span><br/>
                                <sub> 
                                    ${this.neededFilesText.length == 0 ? html `Ready to Convert!!!` : html `Waiting for: ${this.neededFilesText.join(' & ')}` }
                                </sub>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div id="errormsg"> </div>
                    <vaadin-button  
                        @click=${this.ConvertFiles} 
                        id="file-uploader-button" 
                        class="fileuploadBTN"
                        theme="primary">
                        Convert files
                    </vaadin-button>
                </div>
            </div>
                <input type="file" id="fileid" accept=".js,.css" multiple @change=${(e) => this.filesInput(e)}>
            </div>
           
        `
    }

    static get styles() {
        return css`
     
        sub{
          color:#505457;
        }
    
        #fileid{
          display: none;
        }
    
        .container{
          display: flex;
          height: 100%;
        }
    
    
        .uploadText{
          text-align: center;
          margin: auto;
        }
    
        .box{
          width: 98%;
          height: 250px;
          background-color: rgb(216, 216, 216);
          border-color: rgb(69, 69, 69);
          border-width: medium;
          margin: auto;
          border-style: dashed;
          border-radius: 10px;
          color: black;
    
        }
    
        .hover{
          background-color: #fffff;
        }
        
        #field-control-label{
            color: white;
          }
    
          .copypaster{
            width: 400px;
          }
    
          vaadin-text-field{
            width: 90%; 
            padding-top: 10px;
          }
    
          .textcopypaste{
            width:70%;
          
          }
          
          #errormsg{
            display: flex;
            align-items: center;
            justify-content: center;
           
            visibility: hidden;
            z-index: 1; 
          }
        
          #errormsg.show {
            visibility: visible; 
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
          }
    
          @-webkit-keyframes fadein {
            from {height: 0px; opacity: 0;}
            to {height: 100%; opacity: 1;}
          }
          
          @keyframes fadein {
            from {height: 0px; opacity: 0;}
            to {height: 100%; opacity: 1;}
          }
          
          @-webkit-keyframes fadeout {
            from {height: 100%; opacity: 1;}
            to {height: 0px; opacity: 0;}
          }
          
          @keyframes fadeout {
            from {height: 100%; opacity: 1;}
            to {height: 0px; opacity: 0;}
          }
    
          .fileuploadBTN{
            width:100%
          }
    
          #file-download-button{
            display: block;
          }
    
          #download{
           display:none;
          }
    
          .sectionHelper{
            height: 100%;
            border-style: dotted;
            padding: 15px;
            border-radius: 10px;
            margin-top: 20px;
          
          }
          vaadin-multi-select-combo-box-chip {
            background-color: rgb(0, 106, 245) !important;
          }
          
          vaadin-multi-select-combo-box-chip {
              color: white;
              font-size: 0.9em
          }
      
        `
      }

}

window.customElements.define('app-fileupload', APP_FileUpload)