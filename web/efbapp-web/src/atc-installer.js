import { LitElement, css, html } from 'lit'

import {Button} from '@vaadin/button';
import foldericon from './folder.svg'
import {Select} from '@vaadin/select';

import './app-checkbox'
import './app-copyPast'
import './app-fileupload'

export class ATC_Installer extends LitElement {



  constructor() {
    super()
    this.file = []
    this.neededFiles = ["PMDGTablet.js", "PMDGTablet.css"]
    this.neededFilesText = ["PMDGTablet.js", "PMDGTablet.css"]
    this.availableApps = []
    this.appsToinstall = []
    this.items = [
      {
        label: '737-800',
        value: '737-800',
      },
      {
        label: '737-600',
        value: '737-600',
      },
      {
        label: '737-700',
        value: '737-700',
      },
      {
        label: '737-900',
        value: '737-900',
      },
      {
        component: 'hr',
      },
      {
        label: '777-300ER',
        value: '777-300ER',
      },
    ];
  }

  static properties = {
    neededFilesText: {type: Array},
    items: {type: Array},
    selectedText: {type: String},
    shortname: {type: String},
    apps: {type:Array}
  };

 
  createdownload (data, type_of = "application/zip", filename= "EFB_APPs.zip") {
    let downloadHolder = this.shadowRoot.querySelector('#downloadHolder');
    const a = document.createElement("a");
    a.href = URL.createObjectURL(new Blob([data], {
        type: type_of
    }));
    a.setAttribute("download", filename);
    a.setAttribute("id","downloadFileolder")
    downloadHolder.appendChild(a);
    //this._fileReady()
  }


  downloadFiles() {
    const dowloadObject = this.shadowRoot.querySelector('#downloadFileolder')
    dowloadObject.click()
  }
  

  updatedSelect(e) {
    const aircraft = e?.detail.value
   
    const nameAndApps = (aircraft) => {
      switch(aircraft) {
        case '737-600' : 
          return  { shortname:'736',  apps: ["ATC Network"] };
        case '737-700' : 
          return { shortname:'737',  apps: ["ATC Network"] };
        case '737-800' : 
          return { shortname:'738',  apps: ["ATC Network", "CheckList"] };
        case '737-900' : 
          return { shortname:'739',  apps: ["ATC Network"] };
        case '777-300ER' :
           return { shortname:'77w', apps:["ATC Network"] };
        default: return null;
      }
    }

    this.selectedText = aircraft
    this.availableApps = nameAndApps(aircraft).apps
    this.shortname = nameAndApps(aircraft).shortname
    
  }

  _selectedApp(e) {
    this.appsToinstall = e.detail.apps
    this.shadowRoot.querySelector('app-fileupload').appsToinstallStr = JSON.stringify(e.detail.apps)
  }

  _fileReady(file) {
    this.shadowRoot.querySelector('#convert').style.display = "none"
    this.shadowRoot.querySelector('#download').style.display = "block"
    this.createdownload(file)
  }

  render() {
    const shortname = this.shortname
    const fullname = this.selectedText
 
    return html`
      <section class="sectionHelper" id="convert">
        <div>

          <h2>1. Installing Apps for PMDG EFB </h2>
          
          <vaadin-select
            @value-changed = ${(e) => {
              this.updatedSelect(e)
            }}
            label="Select Aircraft"
            .items="${this.items}"
            value="${this.items[0].value}"
          ></vaadin-select>
          
          <div class="selectApp">Select Apps</div>
          
          <app-checkbox 
            .allowedApps="${this.availableApps}" 
            @selectedApps=${this._selectedApp}>
          </app-checkbox>

          <hr/>
          Go to your MSFS Community folder, then: pmdg-aircraft-${shortname} &#x5c;html_ui&#x5c;Pages&#x5c;VCockpit&#x5c;Instruments&#x5c;PMDGTablet&#x5c;pmdg-${fullname}
          <br/>or copy and paste the direct link most appropriate for your sim below into Windows File Explorer.
          <p>

          <!-- CopyPast -->  
          <app-copypast 
            type="top"
            shortname="${shortname}" 
            selectedText="${this.selectedText}" 
            fullname="${fullname}">
          </app-copypast>
          <!-- /CopyPast -->   
          <p>



        Drag and drop the <b>PMDGTablet.js</b> and <b>PMDGTablet.css</b> files into the white box below and click "Convert files."<br/><p>
        </div>
            <app-fileupload 
              appsToinstallStr="[]"
              selectedText= "${fullname}"
              shortname="${shortname}"
              @downloadReady=${(e) => {this._fileReady(e.detail.blob)}} 
            ></app-fileupload>
      </section>

      <section class="sectionHelper" id="download">
        <h2>2. App Installed</h2>
        With the magic of ASTs, you now have my apps installed on your EFB.<br/>
        Now download and unzip the folder, place the <b>pmdg-aircraft-${shortname}</b> folder into your MSFS Community folder, and click Replace All.<br/>
        <br/>
        Don't worry, your original PMDGTablet.js and PMDGTablet.css have been backed up in the download for you.
        <p>
          <!-- CopyPast -->  
          <app-copypast 
            type="bottom"
            shortname="${shortname}" 
            selectedText="${this.selectedText}" 
            fullname="${fullname}">
          </app-copypast>
          <!-- /CopyPast -->   
          
          <vaadin-button 
            @click=${this.downloadFiles} 
            theme="primary success" 
            id="file-download-button" 
            class="fileuploadBTN">
            Download
          </vaadin-button>
          <template id="downloadHolder"></template>
      </section>
    `
  }


  static get styles() {
    return css`
 
    .selectApp{
      margin-top: 10px;
      font-size: 14px;
    }

    sub{
      color:#505457;
    }

    #fileid{
      display: none;
    }

    .container{
      display: flex;
      height: 100%;
    }


    .uploadText{
      text-align: center;
      margin: auto;
    }

    .box{
      width: 98%;
      height: 250px;
      background-color: rgb(216, 216, 216);
      border-color: rgb(69, 69, 69);
      border-width: medium;
      margin: auto;
      border-style: dashed;
      border-radius: 10px;
      color: black;

    }

    .hover{
      background-color: #fffff;
    }
    
    #field-control-label{
        color: white;
      }

      .copypaster{
        width: 400px;
      }

      vaadin-text-field{
        width: 90%; 
        padding-top: 10px;
      }

      .textcopypaste{
        width:70%;
      
      }
      
      #errormsg{
        display: flex;
        align-items: center;
        justify-content: center;
       
        visibility: hidden;
        z-index: 1; 
      }
    
      #errormsg.show {
        visibility: visible; 
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
      }

      @-webkit-keyframes fadein {
        from {height: 0px; opacity: 0;}
        to {height: 100%; opacity: 1;}
      }
      
      @keyframes fadein {
        from {height: 0px; opacity: 0;}
        to {height: 100%; opacity: 1;}
      }
      
      @-webkit-keyframes fadeout {
        from {height: 100%; opacity: 1;}
        to {height: 0px; opacity: 0;}
      }
      
      @keyframes fadeout {
        from {height: 100%; opacity: 1;}
        to {height: 0px; opacity: 0;}
      }

      .fileuploadBTN{
        width:100%
      }

      #file-download-button{
        display: block;
      }

      #download{
       display:none;
      }

      .sectionHelper{
        height: 100%;
        border-style: dotted;
        padding: 15px;
        border-radius: 10px;
        margin-top: 20px;
      
      }
      vaadin-multi-select-combo-box-chip {
        background-color: rgb(0, 106, 245) !important;
      }
      
      vaadin-multi-select-combo-box-chip {
          color: white;
          font-size: 0.9em
      }
  
    `
  }
}

window.customElements.define('atc-installer', ATC_Installer)
