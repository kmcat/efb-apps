import { LitElement, css, html } from 'lit'


export class APP_CheckBox extends LitElement {

    constructor() {
        super()
        this.appsToinstall = []
    }

    static properties = {
        allowedApps: {type: Array},
    }

    change(e) {
        const appsArr = []
        const allCheckboxes = this.shadowRoot.querySelectorAll('.inp-cbx:checked')
        allCheckboxes.forEach(checkedBoxes => {
            appsArr.push(checkedBoxes.value)
        });

        this.appsToinstall = appsArr
        this._dispatchEvent()
    }

    updated(changedProperties) {
        if(changedProperties.has('allowedApps')) {
            const allCheckboxes = this.shadowRoot.querySelectorAll('.inp-cbx:checked')
            allCheckboxes.forEach(checkedBoxes => {
                checkedBoxes.checked = false;
            });
            this.appsToinstall = []
            this._dispatchEvent()
        }
    }

    _dispatchEvent () {
        
        const options = {
            detail: {apps:this.appsToinstall},
            bubbles: true,
            composed: true,
        };

        this.dispatchEvent(new CustomEvent('selectedApps', options));
    }


    render() { 
        
        return html`
        
            <div class="checkbox-wrapper-46">
                ${this.allowedApps.map((app, index) => {
                    return html `
                        <span class="appselectbox">
                            <input class="inp-cbx" id="cbx-${index}" type="checkbox" value="${app}" @change=${e => this.change(e)} />
                            <label class="cbx" for="cbx-${index}">
                                <span>
                                    <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                    </svg>
                                </span>
                                <span>${app}</span>
                            </label>
                        </span>
                    `
                })}
            </div>
        `
    }

    static get styles() {
        return css`

            .appselectbox {
              padding-right: 20px
            }

            .checkbox-wrapper-46 input[type="checkbox"] {
                display: none;
                visibility: hidden;
            }

            .checkbox-wrapper-46 .cbx {
                margin: auto;
                -webkit-user-select: none;
                user-select: none;
                cursor: pointer;
            }

            .checkbox-wrapper-46 .cbx span {
                display: inline-block;
                vertical-align: middle;
                transform: translate3d(0, 0, 0);
            }
  
            .checkbox-wrapper-46 .cbx span:first-child {
            position: relative;
            width: 18px;
            height: 18px;
            border-radius: 3px;
            transform: scale(1);
            vertical-align: middle;
            border: 1px solid #9098A9;
            transition: all 0.2s ease;
            }

            .checkbox-wrapper-46 .cbx span:first-child svg {
                position: absolute;
                top: 3px;
                left: 2px;
                fill: none;
                stroke: #FFFFFF;
                stroke-width: 2;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-dasharray: 16px;
                stroke-dashoffset: 16px;
                transition: all 0.3s ease;
                transition-delay: 0.1s;
                transform: translate3d(0, 0, 0);
            }

            .checkbox-wrapper-46 .cbx span:first-child:before {
                content: "";
                width: 100%;
                height: 100%;
                background: hsl(214, 100%, 48%);
                display: block;
                transform: scale(0);
                opacity: 1;
                border-radius: 50%;
            }

            .checkbox-wrapper-46 .cbx span:last-child {
                padding-left: 8px;
            }

            .checkbox-wrapper-46 .cbx:hover span:first-child {
                border-color: hsl(214, 100%, 48%);
            }

          

            .checkbox-wrapper-46 .inp-cbx:checked + .cbx span:first-child {
                background: hsl(214, 100%, 48%);
                border-color: hsl(214, 100%, 48%);
                animation: wave-46 0.4s ease;
            }

            .checkbox-wrapper-46 .inp-cbx:checked + .cbx span:first-child svg {
                stroke-dashoffset: 0;
            }

            .checkbox-wrapper-46 .inp-cbx:checked + .cbx span:first-child:before {
                transform: scale(3.5);
                opacity: 0;
                transition: all 0.6s ease;
            }

            @keyframes wave-46 {
                50% {
                transform: scale(0.9);
                }
            }
    
        `
    }

}

window.customElements.define('app-checkbox', APP_CheckBox)