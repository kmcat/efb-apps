import { LitElement, css, html, nothing } from 'lit'
import {Button} from '@vaadin/button';
import {TextField} from '@vaadin/text-field';

export class APP_CopyPast extends LitElement {

    constructor() {
        super()
        this.shortname = ""
        this.selectedText = ""
        this.fullname = ""
        this.type = ""
    }

    static properties = {
        shortname: {type: String},
        selectedText: {type: String},
        fullname: {type: String},
        type: {type: String}
    }

    async copytoclipboard(event) {
        const {shortname, selectedText, fullname} = this
        let path = ''
        switch (event) {
          case 'steamFull': path = `%appdata%/Microsoft Flight Simulator/Packages/Community/pmdg-aircraft-${shortname}/html_ui/Pages/VCockpit/Instruments/PMDGTablet/pmdg-${selectedText}`; break;
          case 'steamcom': path = "%appdata%/Microsoft Flight Simulator/Packages/Community/"; break;
          case 'ms-storefull': path = `c:/users/%username%/AppData/Local/Packages/Microsoft.FlightSimulator_8wekyb3d8bbwe/LocalCache/Packages/Community/pmdg-aircraft-${shortname}/html_ui/Pages/VCockpit/Instruments/PMDGTablet/pmdg-${fullname}`; break;
          case 'ms-storecom': path = "c:/users/%username%/AppData/Local/Packages/Microsoft.FlightSimulator_8wekyb3d8bbwe/LocalCache/Packages/Community/"; break;
          default: return null;
        }
         
        await navigator.clipboard.writeText(path);
    }


    render() { 
        const {shortname, selectedText, fullname, type} = this
        if (this.type === "top") {
            return html`

                <vaadin-text-field
                    label="Steam" 
                    class="textcopypaste" 
                    readonly  
                    value="&#x25;appdata&#x25;&#x5c;Microsoft Flight Simulator&#x5c;Packages&#x5c;Community&#x5c;pmdg-aircraft-${shortname}&#x5c;html_ui&#x5c;Pages&#x5c;VCockpit&#x5c;Instruments&#x5c;PMDGTablet&#x5c;pmdg-${fullname}" >
                </vaadin-text-field>
                <vaadin-button @click="${() => this.copytoclipboard("steamFull")}" theme="primary"> Copy </vaadin-button>

                <vaadin-text-field  
                    label="MS-Store" 
                    class="textcopypaste" 
                    readonly  
                    value="c:&#x5c;users&#x5c;&#x25;username&#x25;&#x5c;AppData&#x5c;Local&#x5c;Packages&#x5c;Microsoft.FlightSimulator_8wekyb3d8bbwe&#x5c;LocalCache&#x5c;Packages&#x5c;Community&#x5c;pmdg-aircraft-${shortname}&#x5c;html_ui&#x5c;Pages&#x5c;VCockpit&#x5c;Instruments&#x5c;PMDGTablet&#x5c;pmdg-${fullname}" >
                </vaadin-text-field>  
                <vaadin-button @click="${() => this.copytoclipboard("ms-storefull")}" theme="primary"> Copy </vaadin-button>
            `
        }
        //
        else if (this.type === "bottom") {
           return html`
                <vaadin-text-field 
                    label="Steam" 
                    class="textcopypaste" 
                    readonly  
                    value="&#x25;appdata&#x25;&#x5c;Microsoft Flight Simulator&#x5c;Packages&#x5c;Community" >
                </vaadin-text-field>
                <vaadin-button @click="${() => this.copytoclipboard("steamcom")}" theme="primary"> Copy </vaadin-button>
                
                <vaadin-text-field
                    label="MS-Store" 
                    class="textcopypaste" 
                    readonly  
                    value="c:&#x5c;users&#x5c;&#x25;username&#x25;&#x5c;AppData&#x5c;Local&#x5c;Packages&#x5c;Microsoft.FlightSimulator_8wekyb3d8bbwe&#x5c;LocalCache&#x5c;Packages&#x5c;Community" >
                </vaadin-text-field>
                <vaadin-button @click="${() => this.copytoclipboard("ms-storecom")}" theme="primary"> Copy </vaadin-button>
            `
        }

        else return html `${nothing}`
    }

    static get styles() {
        return css`
            sub{
                color:#505457;
            }

            #fileid{
            display: none;
            }

            .container{
            display: flex;
            height: 100%;
            }

            .uploadText{
                text-align: center;
                margin: auto;
            }

            .box{
                width: 98%;
                height: 250px;
                background-color: rgb(216, 216, 216);
                border-color: rgb(69, 69, 69);
                border-width: medium;
                margin: auto;
                border-style: dashed;
                border-radius: 10px;
                color: black;

            }

            .hover{
                background-color: #fffff;
            }
            
            #field-control-label{
                color: white;
            }

            .copypaster{
                width: 400px;
            }

            vaadin-text-field{
                width: 90%; 
                padding-top: 10px;
            }

            .textcopypaste{
                width:70%;
            
            }
    
        `
    }

}

window.customElements.define('app-copypast', APP_CopyPast)