//import * as recast  from 'recast';
import {parse, visit ,print} from 'recast'


export const ast = async (file, appsToInstall) => {

    try {

        const ast = parse(file, {
        ecmaVersion:2018,
        block: true, 
        })

        const getdata = async (url) => {
            const res = await fetch(url)
            if (!res.ok) throw new Error("Connection to End Failed");
            const code = await res.text()
            return code
        }

        //Add New code0 
            const url = "https://cdn.kmcat.uk/"   

            const atcUrls = [
                `${url}/efb-data/ATC_Networks/newapp.txt`,
                `${url}/efb-data/ATC_Networks/newFSComp.txt`,
                `${url}/efb-data/ATC_Networks/mainupdate.txt`,
                `${url}/efb-data/ATC_Networks/KMC_ATCApp2.txt`,
            ]

            const checkListUrls = [
                `${url}/efb-data/CheckList/newapp.txt`,
                `${url}/efb-data/CheckList/newFSComp.txt`,
                `${url}/efb-data/CheckList/mainupdate.txt`,
                `${url}/efb-data/CheckList/KMC_CheckListApp.txt`,
            ]


            let installUrls  = []
            let apptoAdd = []

            appsToInstall.forEach(app => {
                switch (app) {
                    case 'ATC Network':
                        installUrls.push(atcUrls)
                        apptoAdd.push('fun(this.atc)')
                        break;
                    case 'CheckList':
                        installUrls.push(checkListUrls)
                        apptoAdd.push('fun(this.CheckList)')
                        break;
                }
            });

        console.log({installUrls, apptoAdd})

        visit(ast, 
        {

            visitClassDeclaration: (path) =>  { 

                if(path.node.id.name === "KMCat_CheckList_App") {
                    console.log("CheckList_App Inatalled")
                    installUrls.splice(installUrls .indexOf(atcUrls), 1)
                    apptoAdd.splice(apptoAdd .indexOf("fun(this.atc)"), 1)
                }
            
                if(path.node.id.name === "KMCat_ATC_App") {
                    console.log("ATC installed")
                    installUrls.splice(installUrls .indexOf(checkListUrls), 1)
                    apptoAdd.splice(apptoAdd .indexOf("fun(this.checkList)"), 1)
                }
                return false;
            }
        })

        const codeRes2 = async (list) => await Promise.all(list.map(e => getdata(e)))
        const code = await Promise.all(installUrls.map(async (e) => await codeRes2(e)))
       

        let i = 0;
        visit(ast, 
        {

            visitClassDeclaration: (path) =>  { 

                if(path.node.id.name === "HomeScreen"){
                    let newPath = path.get('body');
                    visit(newPath,{
                        visitMethodDefinition: (path) => {
                            if(path.node.kind === "constructor") {
                                code.map( item => {
                                    const astnew = parse(item[0]);
                                    console.log(astnew)
                                    astnew.program.body.map(e => {
                                        path.node.value.body.body.push(e)
                                    })
                                })
                            }
                    
                            else if(path.node.key.name === "render") {
                                let newPath0 = path.node.value.body
                                visit(newPath0,{
                                    visitObjectExpression: (path) => {
                                        if(path.node.properties[0].value.value === 'col-md-4' && path.parentPath.value.length === 2 && i < apptoAdd.length) {
                                            const newcode = parse(apptoAdd.join('\n'));
                                            path.parentPath.value.push(newcode.program.body[i].expression.arguments[0])
                                            i ++
                                        }          
                                        return false
                                    }
                                })
                            }           
                            return false;
                        }
                    })
                         
                }
                    
                else if(path.node.id.name === "PMDGTablet") {

                    const isUpdate = path.node.body.body.filter((e) => { 
                        if(e.key.name === "Update") return e;
                    })

                    if(isUpdate.length === 0) {
                        const newcode_ = `class Temp { Update() { }}`
                        const newcode = parse(newcode_) 
                        path.node.body.body.push(newcode.program.body[0].body.body[0])
                    }

                    path.node.body.body.filter((e) => { 
                        if(e.key.name === "Update") {
                            code.map( item => {
                                const newcode2 = parse(item[2]) 
                                e.value.body.body.push(...newcode2.program.body[0].body.body[0].value.body.body)
                            })
                        }
                    })

                    let newPath =  path.get('body');
                    visit(newPath, {
                        visitCallExpression(path) {
                            this.traverse(path)
                            if(	typeof path.node.arguments[0] != 'undefined') {
                                if(path.node.arguments[0]) {
                                    if(path.node.arguments[0].name === "EFB"){
                                        code.map( item => {
                                            const newcode = parse(item[1]);
                                            newcode.program.body.map(e => {
                                                path.parentPath.parentPath.parentPath.parentPath.value.push(e)
                                            })
                                        })
                                    }
                                }
                            }
                        }
                    
                    })
                }
                else if(path.node.id.name.includes(["KMCat_ATC_App", "KMCat_ATC_Card", "KMCat_CheckList_App"])) path.prune();
        
                return false
            },
              
        })
        //Add new code
        visit(ast, {
            visitProgram: (path) =>  {
                code.map( item => {
                    const newcode = parse(item[3])
                    newcode.program.body.map(_class => {
                        path.node.body.push(_class)
                    })
                    
                })
                return false
            }
        })

        return print(ast).code;
    }

    catch(err) {
        console.error(err)
        throw err
    }
}